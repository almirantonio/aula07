package com.example.almir.aula07;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void Dialog(View view) {
        Intent i = new Intent(this, Dialog.class);
        startActivity(i);
    }

    public void Bar(View view) {
        Intent i = new Intent(this, Bar.class);
        startActivity(i);
    }

    public void Alerta(View view) {
        Intent i = new Intent(this, Alerta.class);
        startActivity(i);
    }

        public void Lista(View view) {
        Intent i = new Intent(this, listar.class);
        startActivity(i);
   }
}
